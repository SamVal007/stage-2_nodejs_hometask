const { fighter } = require('../models/fighter');
const FighterService = require('../services/fighterService');
const fighterPower = {min:0, max:100}
const fighterDefense = {min:1, max:10}
const createFighterValid = (req, res, next) => {
    try {
        const { name, power, defense } = req.body;

        if(!name) {
            throw new Error('Empty fields should be filled up');
        }

        if(!Number.isInteger(power) || !Number.isInteger(defense)) {
            throw new Error('Power or Defense must be an integer!');
        }

        if(power <= fighterPower.min || power > fighterPower.max) {
            throw new Error('Power should be from 1 till 100');
        }

        if(defense < fighterDefense.min || defense > fighterDefense.max ) {
            throw new Error('Power should be from 1 till 10');
        }

        if(Object.keys(req.body).length == 0) {
            throw new Error('Data is empty');
        }

        next();
    } catch (e) {
        res.status(400).json(e.message);
    }
}

const fighterKeys = Object.keys(fighter);

const updateFighterValid = async (req, res, next) => {
    try {
        const { id } = req.params;
        const fields = Object.keys(req.body);

        if (!fields.every(key => fighterKeys.indexOf(key) >= 0) || req.body.id ||FighterService.findFighterById(id) || Object.keys(fields).length == 0) {
            throw new Error('Data processing error');
        } else
            for (let key in req.body) {
                if (key === 'health' && req.body[key] < 0) {
                    throw new Error('Data processing error');

                }
                if (key === 'defense' && (req.body[key] < 1 || req.body[key] > 10 || isNaN(req.body[key]))) {
                    throw new Error('Data processing error');

                }
                if (key === 'power' && (req.body[key] <= 0 || req.body[key] >= 100 || isNaN(req.body[key]))) {
                    throw new Error('Data processing error');

                }
            }
            next();
    } catch (e) {
        res.status(400).json(e.message);
    }
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;